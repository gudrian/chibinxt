#pragma once

#include "gfx.hpp"

namespace nxtos {

class Image;
class Font;

class Display {
      public:
	static gdispImageError draw(const Image &image, gCoord x, gCoord y, gCoord cx, gCoord cy, gCoord sx, gCoord sy);
	static void draw(const char *text, gCoord y, gCoord cx, gCoord cy, gCoord x, const Font &font, gColor color, gJustify justify);
	static void control(unsigned what, void *value);
	static void invert(bool value);
	static void flush();
};

} // namespace nxtos