#pragma once

#include "gfx.hpp"

namespace nxtos {

class Font {
      public:
	gFont handle() const;
	static Font open(const char *name);

      private:
	Font() = default;

	gFont _font = {};
};

} // namespace nxtos