#pragma once

namespace nxtos {

class Application {
      public:
	Application();

	[[noreturn]] int run();

	static Application &instance();

      private:
	static Application *_instance;
};

} // namespace nxtos