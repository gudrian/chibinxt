#pragma once

#include "gfx.hpp"

namespace nxtos {

class Image {
      public:
	explicit Image() = default;

	gImage *handle() const;

	static Image fromFile(const char *filename);

      private:
	gImage _image = {};
};

} // namespace nxtos