#include "Font.h"

namespace nxtos {

Font Font::open(const char *name)
{
	Font font;
	font._font = gdispOpenFont(name);
	return font;
}

gFont Font::handle() const
{
	return _font;
}

} // namespace nxtos