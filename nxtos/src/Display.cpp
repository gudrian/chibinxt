#include "Display.h"
#include "Font.h"
#include "Image.h"

namespace nxtos {

gdispImageError Display::draw(const Image &image, gCoord x, gCoord y, gCoord cx, gCoord cy, gCoord sx, gCoord sy)
{
	return gdispImageDraw(image.handle(), x, y, cx, cy, sx, sy);
}

void Display::draw(const char *text, gCoord y, gCoord cx, gCoord cy, gCoord x, const Font &font, gColor color, gJustify justify)
{
	gdispDrawStringBox(x, y, cx, cy, text, font.handle(), color, justify);
}

void Display::flush()
{
	gdispFlush();
}

void Display::control(unsigned int what, void *value)
{
	gdispControl(what, value);
}

void Display::invert(bool value)
{
	control(GDISP_CONTROL_INVERSE, value ? GDISP_CONTROL_INVERSE_ON : GDISP_CONTROL_INVERSE_OFF);
}

} // namespace nxtos