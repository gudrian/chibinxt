#include "Application.h"
#include "Display.h"
#include "Font.h"
#include "Image.h"

namespace nxtos {

Application *Application::_instance;

Application::Application()
{
	_instance = this;

	halInit();
	chSysInit();
	nxtInit();
	gfxInit();
}

[[noreturn]] int Application::run()
{
	gCoord width = gdispGGetWidth(GDISP);
	gCoord height = gdispGetHeight();

	Display::invert(true);

	Font font = Font::open("UI2");
	Image image = Image::fromFile("chibios_logo_bw.bmp");
	Display::draw(image, 0, 0, width, height, 0, 0);
	Display::draw(CH_KERNEL_VERSION, 49, 39, 15, 60, font, GFX_BLACK, gJustifyRight);
	Display::flush();

	bool inv = false;
	while (true) {
		palReadPad(IOPORT1, 0);
		Display::invert(inv);
		inv = !inv;
		chThdSleepSeconds(1);
	}
}
Application &Application::instance()
{
	return *_instance;
}

} // namespace nxtos