#include "Image.h"

namespace nxtos {

Image Image::fromFile(const char *filename)
{
	Image image;
	gdispImageOpenFile(&image._image, filename);
	return image;
}

gImage *Image::handle() const
{
	return const_cast<gImage *>(&_image);
}

} // namespace nxtos