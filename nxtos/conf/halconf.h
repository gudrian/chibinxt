#pragma once

#define _CHIBIOS_HAL_CONF_
#define _CHIBIOS_HAL_CONF_VER_7_1_

#include "mcuconf.h"

/**
 * @brief   Enables the PAL subsystem.
 */
#define HAL_USE_PAL TRUE

/**
 * @brief   Enables the ADC subsystem.
 */
#define HAL_USE_ADC FALSE

/**
 * @brief   Enables the CAN subsystem.
 */
#define HAL_USE_CAN FALSE

/**
 * @brief   Enables the cryptographic subsystem.
 */
#define HAL_USE_CRY FALSE

/**
 * @brief   Enables the DAC subsystem.
 */
#define HAL_USE_DAC FALSE

/**
 * @brief   Enables the EFlash subsystem.
 */
#define HAL_USE_EFL FALSE

/**
 * @brief   Enables the GPT subsystem.
 */
#define HAL_USE_GPT FALSE

/**
 * @brief   Enables the I2C subsystem.
 */
#define HAL_USE_I2C TRUE

/**
 * @brief   Enables the I2S subsystem.
 */
#define HAL_USE_I2S FALSE

/**
 * @brief   Enables the ICU subsystem.
 */
#define HAL_USE_ICU FALSE

/**
 * @brief   Enables the MAC subsystem.
 */
#define HAL_USE_MAC FALSE

/**
 * @brief   Enables the MMC_SPI subsystem.
 */
#define HAL_USE_MMC_SPI FALSE

/**
 * @brief   Enables the PWM subsystem.
 */
#define HAL_USE_PWM FALSE

/**
 * @brief   Enables the RTC subsystem.
 */
#define HAL_USE_RTC FALSE

/**
 * @brief   Enables the SDC subsystem.
 */
#define HAL_USE_SDC FALSE

/**
 * @brief   Enables the SERIAL subsystem.
 */
#define HAL_USE_SERIAL FALSE

/**
 * @brief   Enables the SERIAL over USB subsystem.
 */
#define HAL_USE_SERIAL_USB FALSE

/**
 * @brief   Enables the SIO subsystem.
 */
#define HAL_USE_SIO FALSE

/**
 * @brief   Enables the SPI subsystem.
 */
#define HAL_USE_SPI FALSE

/**
 * @brief   Enables the TRNG subsystem.
 */
#define HAL_USE_TRNG FALSE

/**
 * @brief   Enables the UART subsystem.
 */
#define HAL_USE_UART FALSE

/**
 * @brief   Enables the USB subsystem.
 */
#define HAL_USE_USB FALSE

/**
 * @brief   Enables the WDG subsystem.
 */
#define HAL_USE_WDG FALSE

/**
 * @brief   Enables the WSPI subsystem.
 */
#define HAL_USE_WSPI FALSE

/*===========================================================================*/
/* PAL driver related settings.                                              */
/*===========================================================================*/

/**
 * @brief   Enables synchronous APIs.
 * @note    Disabling this option saves both code and data space.
 */
#define PAL_USE_CALLBACKS FALSE

/**
 * @brief   Enables synchronous APIs.
 * @note    Disabling this option saves both code and data space.
 */
#define PAL_USE_WAIT FALSE

/*===========================================================================*/
/* ADC driver related settings.                                              */
/*===========================================================================*/

/**
 * @brief   Enables synchronous APIs.
 * @note    Disabling this option saves both code and data space.
 */
#define ADC_USE_WAIT FALSE

/**
 * @brief   Enables the @p adcAcquireBus() and @p adcReleaseBus() APIs.
 * @note    Disabling this option saves both code and data space.
 */
#define ADC_USE_MUTUAL_EXCLUSION FALSE

/*===========================================================================*/
/* CAN driver related settings.                                              */
/*===========================================================================*/

/**
 * @brief   Sleep mode related APIs inclusion switch.
 */
#define CAN_USE_SLEEP_MODE FALSE

/**
 * @brief   Enforces the driver to use direct callbacks rather than OSAL events.
 */
#define CAN_ENFORCE_USE_CALLBACKS FALSE

/*===========================================================================*/
/* CRY driver related settings.                                              */
/*===========================================================================*/

/**
 * @brief   Enables the SW fall-back of the cryptographic driver.
 * @details When enabled, this option, activates a fall-back software
 *          implementation for algorithms not supported by the underlying
 *          hardware.
 * @note    Fall-back implementations may not be present for all algorithms.
 */
#define HAL_CRY_USE_FALLBACK FALSE

/**
 * @brief   Makes the driver forcibly use the fall-back implementations.
 */
#define HAL_CRY_ENFORCE_FALLBACK FALSE

/*===========================================================================*/
/* DAC driver related settings.                                              */
/*===========================================================================*/

/**
 * @brief   Enables synchronous APIs.
 * @note    Disabling this option saves both code and data space.
 */
#define DAC_USE_WAIT TRUE

/**
 * @brief   Enables the @p dacAcquireBus() and @p dacReleaseBus() APIs.
 * @note    Disabling this option saves both code and data space.
 */
#define DAC_USE_MUTUAL_EXCLUSION TRUE

/*===========================================================================*/
/* I2C driver related settings.                                              */
/*===========================================================================*/

/**
 * @brief   Enables the mutual exclusion APIs on the I2C bus.
 */
#define I2C_USE_MUTUAL_EXCLUSION TRUE

/*===========================================================================*/
/* MAC driver related settings.                                              */
/*===========================================================================*/

/**
 * @brief   Enables the zero-copy API.
 */
#define MAC_USE_ZERO_COPY TRUE

/**
 * @brief   Enables an event sources for incoming packets.
 */
#define MAC_USE_EVENTS TRUE

/*===========================================================================*/
/* MMC_SPI driver related settings.                                          */
/*===========================================================================*/

/**
 * @brief   Delays insertions.
 * @details If enabled this options inserts delays into the MMC waiting
 *          routines releasing some extra CPU time for the threads with
 *          lower priority, this may slow down the driver a bit however.
 *          This option is recommended also if the SPI driver does not
 *          use a DMA channel and heavily loads the CPU.
 */
#define MMC_NICE_WAITING TRUE

/*===========================================================================*/
/* SDC driver related settings.                                              */
/*===========================================================================*/

/**
 * @brief   Number of initialization attempts before rejecting the card.
 * @note    Attempts are performed at 10mS intervals.
 */
#define SDC_INIT_RETRY 100

/**
 * @brief   Include support for MMC cards.
 * @note    MMC support is not yet implemented so this option must be kept
 *          at @p FALSE.
 */
#define SDC_MMC_SUPPORT TRUE

/**
 * @brief   Delays insertions.
 * @details If enabled this options inserts delays into the MMC waiting
 *          routines releasing some extra CPU time for the threads with
 *          lower priority, this may slow down the driver a bit however.
 */
#define SDC_NICE_WAITING TRUE

/**
 * @brief   OCR initialization constant for V20 cards.
 */
#define SDC_INIT_OCR_V20 0x50FF8000U

/**
 * @brief   OCR initialization constant for non-V20 cards.
 */
#define SDC_INIT_OCR 0x80100000U

/*===========================================================================*/
/* SERIAL driver related settings.                                           */
/*===========================================================================*/

/**
 * @brief   Default bit rate.
 * @details Configuration parameter, this is the baud rate selected for the
 *          default configuration.
 */
#define SERIAL_DEFAULT_BITRATE 38400

/**
 * @brief   Serial buffers size.
 * @details Configuration parameter, you can change the depth of the queue
 *          buffers depending on the requirements of your application.
 * @note    The default is 16 bytes for both the transmission and receive
 *          buffers.
 */
#define SERIAL_BUFFERS_SIZE 16

/*===========================================================================*/
/* SERIAL_USB driver related setting.                                        */
/*===========================================================================*/

/**
 * @brief   Serial over USB buffers size.
 * @details Configuration parameter, the buffer size must be a multiple of
 *          the USB data endpoint maximum packet size.
 * @note    The default is 256 bytes for both the transmission and receive
 *          buffers.
 */
#define SERIAL_USB_BUFFERS_SIZE 256

/**
 * @brief   Serial over USB number of buffers.
 * @note    The default is 2 buffers.
 */
#define SERIAL_USB_BUFFERS_NUMBER 2

/*===========================================================================*/
/* SPI driver related settings.                                              */
/*===========================================================================*/

/**
 * @brief   Enables synchronous APIs.
 * @note    Disabling this option saves both code and data space.
 */
#define SPI_USE_WAIT TRUE

/**
 * @brief   Enables circular transfers APIs.
 * @note    Disabling this option saves both code and data space.
 */
#define SPI_USE_CIRCULAR FALSE

/**
 * @brief   Enables the @p spiAcquireBus() and @p spiReleaseBus() APIs.
 * @note    Disabling this option saves both code and data space.
 */
#define SPI_USE_MUTUAL_EXCLUSION TRUE

/**
 * @brief   Handling method for SPI CS line.
 * @note    Disabling this option saves both code and data space.
 */
#define SPI_SELECT_MODE SPI_SELECT_MODE_PAD

/*===========================================================================*/
/* UART driver related settings.                                             */
/*===========================================================================*/

/**
 * @brief   Enables synchronous APIs.
 * @note    Disabling this option saves both code and data space.
 */
#define UART_USE_WAIT TRUE

/**
 * @brief   Enables the @p uartAcquireBus() and @p uartReleaseBus() APIs.
 * @note    Disabling this option saves both code and data space.
 */
#define UART_USE_MUTUAL_EXCLUSION TRUE

/*===========================================================================*/
/* USB driver related settings.                                              */
/*===========================================================================*/

/**
 * @brief   Enables synchronous APIs.
 * @note    Disabling this option saves both code and data space.
 */
#define USB_USE_WAIT TRUE

/*===========================================================================*/
/* WSPI driver related settings.                                             */
/*===========================================================================*/

/**
 * @brief   Enables synchronous APIs.
 * @note    Disabling this option saves both code and data space.
 */
#define WSPI_USE_WAIT TRUE

/**
 * @brief   Enables the @p wspiAcquireBus() and @p wspiReleaseBus() APIs.
 * @note    Disabling this option saves both code and data space.
 */
#define WSPI_USE_MUTUAL_EXCLUSION TRUE
