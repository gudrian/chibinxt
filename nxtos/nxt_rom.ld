/*
    ChibiOS - Copyright (C) 2006..2016 Giovanni Di Sirio

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

MEMORY
{
             ram : org = 0x00000000, len = 1M
           flash : org = 0x00100000, len = 256k
      vector_ram : org = 0x00200000, len = 0x40
            sram : org = 0x00200040, len = 64k - 0x40
}

__stacks_total_size__	    = __und_stack_size__ + __abt_stack_size__ + __fiq_stack_size__ + __irq_stack_size__ + __svc_stack_size__ + __sys_stack_size__;

__ram_start__ = ORIGIN(ram);

__sram_start__ = ORIGIN(sram);
__sram_size__  = LENGTH(sram);
__sram_end__   = __sram_start__ + __sram_size__;

ENTRY(Reset_Handler)

SECTIONS
{
    . = 0;
    _text = .;

    startup : ALIGN(16) SUBALIGN(16)
    {
        KEEP(*(.vectors))
        . = LENGTH(vector_ram);
    } > vector_ram AT> flash

    __vectors_ram_start__  = ADDR(startup);
    __vectors_load_start__ = LOADADDR(startup);
    __vectors_load_end__   = __vectors_load_start__ + SIZEOF(startup);


    constructors : ALIGN(4) SUBALIGN(4)
    {
        PROVIDE(__init_array_start = .);
        KEEP(*(SORT(.init_array.*)))
        KEEP(*(.init_array))
        PROVIDE(__init_array_end = .);
    } > flash

    destructors : ALIGN(4) SUBALIGN(4)
    {
        PROVIDE(__fini_array_start = .);
        KEEP(*(.fini_array))
        KEEP(*(SORT(.fini_array.*)))
        PROVIDE(__fini_array_end = .);
    } > flash

    .text : ALIGN(16) SUBALIGN(16)
    {
        *(.text)
        *(.text.*)
        *(.rodata)
        *(.rodata.*)
        *(.glue_7t)
        *(.glue_7)
        *(.gcc*)
    } > flash

    .ARM.extab :
    {
        *(.ARM.extab* .gnu.linkonce.armextab.*)
    } > flash

    .ARM.exidx : {
        PROVIDE(__exidx_start = .);
        *(.ARM.exidx* .gnu.linkonce.armexidx.*)
        PROVIDE(__exidx_end = .);
     } > flash

    .eh_frame_hdr :
    {
        *(.eh_frame_hdr)
    } > flash

    .eh_frame : ONLY_IF_RO
    {
        *(.eh_frame)
    } > flash

    .textalign : ONLY_IF_RO
    {
        . = ALIGN(8);
    } > flash

    . = ALIGN(4);
    _etext = .;

    .stacks :
    {
        . = ALIGN(8);
        __stacks_base__ = .;
        __main_thread_stack_base__ = .;
        . += __stacks_total_size__;
        . = ALIGN(8);
        __stacks_end__ = .;
    } > sram

    .data : ALIGN(4)
    {
        . = ALIGN(4);
        PROVIDE(_data = .);
        __textdata_base__ = LOADADDR(.data);
        __data_base__ = .;
        *(.data)
        *(.data.*)
        *(.ramtext)
        . = ALIGN(4);
        PROVIDE(_edata = .);
        __data_end__ = .;
    } > sram AT> flash

    .bss : ALIGN(4)
    {
        . = ALIGN(4);
        __bss_base__ = .;
        PROVIDE(_bss_start = .);
        *(.bss)
        *(.bss.*)
        *(COMMON)
        . = ALIGN(4);
        __bss_end__ = .;
        PROVIDE(_bss_end = .);
        PROVIDE(end = .);
    } > sram

    .ram (NOLOAD) : ALIGN(4)
    {
        . = ALIGN(4);
        *(.ram)
        *(.ram.*)
        . = ALIGN(4);
        __sram_free__ = .;
    } > sram
}

/* Heap default boundaries, it is defaulted to be the non-used part
   of ram region.*/
__heap_base__   = __sram_free__;
__heap_end__    = __sram_end__;
