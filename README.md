# [ChibiOS](http://www.chibios.org) on the Lego Mindstorms NXT

This is a port of ChibiOS to the Lego Mindstorm NXT.

I've mainly used existing code from the ChibiOS 2.6 branch (which still supported the AT91SAM7 CPU) and the [nxtOSEK/leJOS](http://lejos-osek.sourceforge.net) projects (Thanks!). The code has been adjusted as needed, though.

# Caveats

* Currently the compiled binary is targeted for SRAM execution. After the NXT has been put into SAMBA mode, the `ramboot` utility of nxtOSEK can be used to transfer and run the binary.
* The only supported compiler is GCC. That probably won't change.

# Todo
* ~~implement communication with the AVR co-processor (needed for power management)~~
* ~~implement tickless mode~~ Won't probably happen since the AT91SAM7 lacks the necessary timer.
* support flash installation
* ~~implement basic [µGFX](http://www.ugfx.io) support~~
