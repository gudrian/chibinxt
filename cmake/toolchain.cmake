set(GNUARM_ROOT "" CACHE PATH "Path to the GNU Arm Embedded Toolchain")

set(target arm-none-eabi)

find_program(
        arm_c_compiler
        NAMES ${target}-gcc
        HINTS ${GNUARM_ROOT}/bin
)

find_program(
        arm_cxx_compiler
        NAMES ${target}-g++
        HINTS ${GNUARM_ROOT}/bin
)

find_program(
        arm_ar
        NAMES ${target}-gcc-ar
        HINTS ${GNUARM_ROOT}/bin
)

find_program(
        arm_size
        NAMES ${target}-size
        HINTS ${GNUARM_ROOT}/bin
)

if (arm_c_compiler STREQUAL "arm_c_compiler-NOTFOUND")
    message(FATAL_ERROR "Unable to find ${target}-gcc")
endif ()
if (arm_cxx_compiler STREQUAL "arm_cxx_compiler-NOTFOUND")
    message(FATAL_ERROR "Unable to find ${target}-g++")
endif ()
if (arm_ar STREQUAL "arm_ar-NOTFOUND")
    message(FATAL_ERROR "Unable to find ${target}-ar")
endif ()
if (arm_size STREQUAL "arm_size-NOTFOUND")
    message(FATAL_ERROR "Unable to find ${target}-size")
endif ()

set(CMAKE_C_COMPILER ${arm_c_compiler})
set(CMAKE_CXX_COMPILER ${arm_cxx_compiler})
set(CMAKE_AR ${arm_ar})
set(CMAKE_SIZE ${arm_size})

set(CMAKE_SYSTEM_NAME Generic)
set(CMAKE_SYSTEM_PROCESSOR arm7tdmi)

# We need to set these to support LTO with static libraries.
set(CMAKE_C_ARCHIVE_CREATE "<CMAKE_AR> qcs <TARGET> <LINK_FLAGS> <OBJECTS>")
set(CMAKE_C_ARCHIVE_FINISH "")
set(CMAKE_CXX_ARCHIVE_CREATE "<CMAKE_AR> qcs <TARGET> <LINK_FLAGS> <OBJECTS>")
set(CMAKE_CXX_ARCHIVE_FINISH "")

# For CMake's compiler tests to succeed we need to use the nosys.specs.
# The firmware itself however will be built with nano.specs since that
# leads to smaller binaries.
set(compiler_flags "-mcpu=arm7tdmi -specs=nosys.specs")
set(CMAKE_C_FLAGS ${compiler_flags})
set(CMAKE_CXX_FLAGS ${compiler_flags})
set(CMAKE_ASM_FLAGS ${compiler_flags})
