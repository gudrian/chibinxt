#include <gfx.h>

#if GFX_USE_GDISP

#if defined(GDISP_SCREEN_HEIGHT)
#warning "GDISP: This low level driver does not support setting a screen size. It is being ignored."
#undef GDISP_SCREEN_HEIGHT
#endif

#if defined(GDISP_SCREEN_WIDTH)
#warning "GDISP: This low level driver does not support setting a screen size. It is being ignored."
#undef GDISP_SCREEN_WIDTH
#endif

#define GDISP_DRIVER_VMT GDISPVMT_MINDSTORMS_NXT
#include <gdisp/gdisp_driver.h>

#ifndef GDISP_INITIAL_CONTRAST
#define GDISP_INITIAL_CONTRAST 51
#endif

#include "include/nxt_lcd.h"

#define GDISP_SCREEN_WIDTH 132
#define GDISP_VISIBLE_SCREEN_WIDTH 100
#define GDISP_SCREEN_HEIGHT 64
#define GDISP_SCREEN_PAGES (GDISP_SCREEN_HEIGHT / 8)

#define GDISP_FLG_NEEDFLUSH (GDISP_FLG_DRIVER << 0)

static uint8_t buffer[GDISP_SCREEN_WIDTH * GDISP_SCREEN_PAGES];

#define xyaddr(x, y) ((x) + ((y) >> 3) * GDISP_SCREEN_WIDTH)
#define xybit(y) (1 << ((y)&7))

LLDSPEC gBool gdisp_lld_init(GDisplay *g)
{
  g->g.Width = GDISP_VISIBLE_SCREEN_WIDTH;
  g->g.Height = GDISP_SCREEN_HEIGHT;
  g->g.Orientation = gOrientation0;
  g->g.Powermode = gPowerOn;
  g->g.Backlight = 0;
  g->g.Contrast = GDISP_INITIAL_CONTRAST;

  nxt_lcd_init(buffer);

  return TRUE;
}

#if GDISP_NEED_CONTROL && GDISP_HARDWARE_CONTROL
LLDSPEC void gdisp_lld_control(GDisplay *g)
{
  switch (g->p.x) {
  case GDISP_CONTROL_INVERSE:
    nxt_lcd_inverse_display((int)g->p.ptr);
    break;
  }
}
#endif

#if GDISP_HARDWARE_FLUSH
LLDSPEC void gdisp_lld_flush(GDisplay *g)
{
  // Don't flush if we don't need it.
  if (!(g->flags & GDISP_FLG_NEEDFLUSH))
    return;

  nxt_lcd_update();

  g->flags &= ~GDISP_FLG_NEEDFLUSH;
}
#endif

#if GDISP_HARDWARE_DRAWPIXEL
LLDSPEC void gdisp_lld_draw_pixel(GDisplay *g)
{
  gCoord x, y;

  switch (g->g.Orientation) {
  default:
  case gOrientation0:
    x = g->p.x;
    y = g->p.y;
    break;
  case gOrientation90:
    x = g->p.y;
    y = GDISP_SCREEN_HEIGHT - 1 - g->p.x;
    break;
  case gOrientation180:
    x = GDISP_SCREEN_WIDTH - 1 - g->p.x;
    y = GDISP_SCREEN_HEIGHT - 1 - g->p.y;
    break;
  case gOrientation270:
    x = GDISP_SCREEN_HEIGHT - 1 - g->p.y;
    y = g->p.x;
    break;
  }

  if (gdispColor2Native(g->p.color) != GFX_BLACK)
    buffer[xyaddr(x, y)] |= xybit(y);
  else
    buffer[xyaddr(x, y)] &= ~xybit(y);

  g->flags |= GDISP_FLG_NEEDFLUSH;
}
#endif // GDISP_HARDWARE_DRAWPIXEL

#endif // GFX_USE_GDISP
