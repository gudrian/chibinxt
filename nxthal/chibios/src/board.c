#include "hal.h"
#include "include/nxt_avr.h"
#include "include/nxt_motors.h"
#include "include/nxt_spi.h"
#include "include/sound.h"

#if HAL_USE_PAL || defined(__DOXYGEN__)
const PALConfig pal_default_config =
    {};
#endif

extern uint32_t __ram_start__;
extern uint32_t __vectors_ram_start__;
extern uint32_t __vectors_load_start__;
extern uint32_t __vectors_load_end__;

static THD_WORKING_AREA(wa_playJingle, 128);
static THD_FUNCTION(playJingle, arg)
{
	(void)arg;

	for (int i = 1; i <= 10; ++i) {
		sound_freq(i * 110, 100, 10);
		chThdSleepMilliseconds(100);
	}
}

static THD_WORKING_AREA(wa_updateThread, 128);
static THD_FUNCTION(updateThread, arg)
{
	(void)arg;

	chRegSetThreadName("update");

	systime_t prev = chVTGetSystemTime();
	while (true) {
		nxt_avr_1kHz_update();
		nxt_motor_1kHz_process();
		prev = chThdSleepUntilWindowed(prev, prev + TIME_MS2I(1));
	}
}

/**
 * Make sure the SRAM is mapped at 0x0 so that the
 * correct vectors are used.
 */
static void map_sram(void)
{
	uint32_t *org = &__ram_start__;
	uint32_t *ram = &__vectors_ram_start__;

	uint32_t before = *org;
	*ram = ~*ram;
	uint32_t after = *org;
	*ram = ~*ram;

	if (before == after) {
		/* The value at 0x0 has not changed which means
     * that the SRAM is not mapped.
     */
		at91sam7_remap();
	}
}

/**
 * Copy the shadow vector table.
 */
static void copy_vectors(void)
{
	uint32_t *src = &__vectors_load_start__;
	uint32_t *dst = &__vectors_ram_start__;
	while (src < &__vectors_load_end__)
		*dst++ = *src++;
}

void __early_init(void)
{
	at91sam7_clock_init();
	at91sam7_watchdog_disable();
	copy_vectors();
	map_sram();
}

/**
 * @brief   Board-specific initialization code.
 * @todo    Add your board-specific code, if any.
 */
void boardInit(void)
{
	nxt_spi_init();
	sound_init();
	nxt_avr_init();
	nxt_motor_init();
}

void nxtInit(void)
{
	chThdCreateStatic(wa_updateThread, sizeof(wa_updateThread), LOWPRIO, updateThread, NULL);
	chThdCreateStatic(wa_playJingle, sizeof(wa_playJingle), HIGHPRIO, playJingle, NULL);
}
