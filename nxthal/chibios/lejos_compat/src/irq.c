#include "irq.h"

extern void twi_isr_C(void);
extern void sound_isr_C(void);
extern void nxt_motor_isr_C(void);
extern void udp_isr_C(void);

OSAL_IRQ_HANDLER(twi_isr_entry)
{
  OSAL_IRQ_PROLOGUE();

  osalSysLockFromISR();
  twi_isr_C();
  osalSysUnlockFromISR();

  AT91C_BASE_AIC->AIC_EOICR = 0;
  OSAL_IRQ_EPILOGUE();
}

OSAL_IRQ_HANDLER(sound_isr_entry)
{
  OSAL_IRQ_PROLOGUE();

  osalSysLockFromISR();
  sound_isr_C();
  osalSysUnlockFromISR();

  AT91C_BASE_AIC->AIC_EOICR = 0;
  OSAL_IRQ_EPILOGUE();
}

OSAL_IRQ_HANDLER(nxt_motor_isr_entry)
{
  OSAL_IRQ_PROLOGUE();

  osalSysLockFromISR();
  nxt_motor_isr_C();
  osalSysUnlockFromISR();

  AT91C_BASE_AIC->AIC_EOICR = 0;
  OSAL_IRQ_EPILOGUE();
}

OSAL_IRQ_HANDLER(udp_isr_entry)
{
  OSAL_IRQ_PROLOGUE();

  osalSysLockFromISR();
  udp_isr_C();
  osalSysUnlockFromISR();

  AT91C_BASE_AIC->AIC_EOICR = 0;
  OSAL_IRQ_EPILOGUE();
}