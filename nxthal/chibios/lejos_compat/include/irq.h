#ifndef __IRQ_H__
#define __IRQ_H__

#include "hal.h"

OSAL_IRQ_HANDLER(twi_isr_entry);
OSAL_IRQ_HANDLER(sound_isr_entry);
OSAL_IRQ_HANDLER(nxt_motor_isr_entry);
OSAL_IRQ_HANDLER(udp_isr_entry);

#endif
