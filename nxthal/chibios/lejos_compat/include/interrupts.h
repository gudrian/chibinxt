#ifndef __INTERRUPTS_H__
#define __INTERRUPTS_H__

#include "aic.h"

#define AIC_INT_LEVEL_LOWEST 1
#define AIC_INT_LEVEL_LOW 2
#define AIC_INT_LEVEL_NORMAL 4
#define AIC_INT_LEVEL_ABOVE_NORMAL 5

#define interrupts_get_and_disable() 0
#define interrupts_enable() \
  {                         \
  }

#endif
