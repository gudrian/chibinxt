#ifndef __MYTYPES_H__
#define __MYTYPES_H__

#include "hal.h"
#include "aic.h"

typedef uint32_t U32;
typedef uint8_t U8;
typedef int32_t S32;
typedef uint16_t U16;
typedef int8_t S8;
typedef uint8_t byte;

#define CLOCK_FREQUENCY MCK

#define aic_mask_off AIC_DisableIT
#define aic_mask_on AIC_EnableIT
#define aic_set_vector(source, mode, handler) AIC_ConfigureIT(source, mode, handler)
#define aic_clear(source) AT91C_BASE_AIC->AIC_ICCR = 1 << (source)

#endif
