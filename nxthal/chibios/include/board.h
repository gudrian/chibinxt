#ifndef __BOARD_H__
#define __BOARD_H__

#define BOARD_LEGO_MINDSTORMS_NXT

/*
 * Select your platform by modifying the following line.
 */
#if !defined(SAM7_PLATFORM)
#define SAM7_PLATFORM SAM7S256
#endif

#include "at91sam7.h"

#define CLK 18432000
#define MCK 48054857

#if !defined(_FROM_ASM_)
#ifdef __cplusplus
extern "C" {
#endif

void boardInit(void);
void nxtInit(void);

#ifdef __cplusplus
}
#endif
#endif

#endif
