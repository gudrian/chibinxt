
#include "include/nxt_spi.h"
#include "ch.h"
#include "hal.h"

#include "aic.h"

#include <string.h>

/*
 * Note that this is not a normal SPI interface,
 * it is a bodged version as used by the NXT's
 * display.
 *
 * The display does not use MISO because you can
 * only write to it in serial mode.
 *
 * Instead, the MISO pin is not used by the SPI
 * and is instead driven as a PIO pin for controlling CD.
 *
 * Addional notes from Andy Shaw
 * The following code now contains the capability to perform display
 * updates using dma, This code was inspired by the nxos lcd/spi code
 * (Thanks guys). More details of nxos can be found at:
 * http://nxt.natulte.net/nxos/trac
 *
 */

#define CS_PIN (1 << 10)
#define CD_PIN (1 << 12)

#define SPI_BITRATE 2000000

const uint8_t *display;
volatile uint8_t dirty;
volatile uint8_t page;
uint8_t mode = 0xff;

static void spi_set_mode(uint8_t m)
{

  uint32_t status;

  /* nothing to do if we are already in the correct mode */
  if (m == mode)
    return;

  /* Wait until all bytes have been sent */
  do {
    status = *AT91C_SPI_SR;
  } while (!(status & 0x200));
  /* Set command or data mode */
  if (m)
    *AT91C_PIOA_SODR = CD_PIN;
  else
    *AT91C_PIOA_CODR = CD_PIN;
  /* remember the current mode */
  mode = m;
}

// static uint8_t transmit_buffer[8 * 132];

static void spiHandlerI(void)
{
  if (!dirty)
    return;

  dirty = 0;

  /* Make sure we are in data mode */
  spi_set_mode(1);

  // memcpy(transmit_buffer, display, 8 * 132);

  *AT91C_SPI_TNPR = (uint32_t)display;
  *AT91C_SPI_TNCR = 8 * 132;

  *AT91C_SPI_IDR = AT91C_SPI_ENDTX;
}

OSAL_IRQ_HANDLER(spi_isr_C)
{
  OSAL_IRQ_PROLOGUE();

  osalSysLockFromISR();
  spiHandlerI();
  osalSysUnlockFromISR();

  AT91C_BASE_AIC->AIC_EOICR = 0;
  OSAL_IRQ_EPILOGUE();
}

void nxt_spi_init(void)
{
  chSysLock();

  *AT91C_PMC_PCER = (1L << AT91C_ID_SPI); /* Enable MCK clock     */

  *AT91C_PIOA_PER = AT91C_PIO_PA12;
  *AT91C_PIOA_OER = AT91C_PIO_PA12;
  *AT91C_PIOA_CODR = AT91C_PIO_PA12;

  *AT91C_PIOA_ASR = AT91C_PA14_SPCK;
  *AT91C_PIOA_ASR = AT91C_PA13_MOSI;
  *AT91C_PIOA_BSR = AT91C_PA10_NPCS2;

  uint32_t pins = AT91C_PA14_SPCK | AT91C_PA13_MOSI | AT91C_PA10_NPCS2;
  *AT91C_PIOA_PDR = pins;
  *AT91C_PIOA_ODR = pins;
  *AT91C_PIOA_OWER = pins;
  *AT91C_PIOA_MDDR = pins;
  *AT91C_PIOA_PPUDR = pins;
  *AT91C_PIOA_IFDR = pins;
  *AT91C_PIOA_CODR = pins;
  *AT91C_PIOA_IDR = pins;

  *AT91C_SPI_CR = AT91C_SPI_SWRST; /*Softreset*/
  *AT91C_SPI_CR = AT91C_SPI_SPIEN; /*Enablespi*/

  *AT91C_SPI_MR = AT91C_SPI_MSTR | AT91C_SPI_MODFDIS | (0xB << 16);

  AT91C_SPI_CSR[2] = ((MCK / SPI_BITRATE) << 8) | AT91C_SPI_CPOL;

  /* Set mode to unknown */
  mode = 0xff;

  /* Set up safe dma refresh state */
  display = NULL;
  dirty = 0;
  page = 0;

  /* Install the interrupt handler */
  AIC_ConfigureIT(AT91C_ID_SPI, AT91C_AIC_PRIOR_HIGHEST - 3, spi_isr_C);
  AIC_EnableIT(AT91C_ID_SPI);

  *AT91C_SPI_PTCR = AT91C_PDC_TXTEN;

  chSysUnlock();
}

void nxt_spi_write(uint32_t CD, const uint8_t *data, uint32_t nBytes)
{
  uint32_t status;
  uint32_t cd_mask = (CD ? 0x100 : 0);

  spi_set_mode(CD);
  while (nBytes) {
    *AT91C_SPI_TDR = (*data | cd_mask);
    data++;
    nBytes--;
    /* Wait until byte sent */
    do {
      status = *AT91C_SPI_SR;
    } while (!(status & 0x200));
  }
}

void nxt_spi_set_display(const uint8_t *disp)
{
  /* Set the display buffer to be used for dma refresh.
   * it is really only safe to set the display once. Should probably
   * sort this out so that it is set separately from requesting a refresh
   */
  if (!display)
    display = disp;
}

void nxt_spi_refresh(void)
{
  /* Request the start of a dma refresh of the display
   */
  // If the display is not set nothing to do.
  if (!display)
    return;
  // Say we have changes
  dirty = 1;
  // Start the DMA refresh
  *AT91C_SPI_IER = AT91C_SPI_ENDTX;
}
