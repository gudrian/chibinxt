#ifndef __SYSTICK_H__
#define __SYSTICK_H__

#include "chtypes.h"

#define systick_wait_ns(time)              \
  do {                                     \
    volatile uint32_t x = (time >> 7) + 1; \
    while (x) {                            \
      x--;                                 \
    }                                      \
  } while (0)

#define systick_wait_ms chThdSleepMilliseconds

#endif
