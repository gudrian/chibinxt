#ifndef __NXT_LCD_H__
#define __NXT_LCD_H__

#include "chtypes.h"

#define NXT_LCD_WIDTH 132
#define NXT_LCD_DEPTH 8

void nxt_lcd_init(uint8_t *disp);
void nxt_lcd_power_up(void);
void nxt_lcd_power_down(void);
void nxt_lcd_update(void);
void nxt_lcd_force_update(void);
void nxt_lcd_set_pot(uint32_t val);
void nxt_lcd_enable(uint32_t on);
void nxt_lcd_inverse_display(uint32_t on);

#endif
